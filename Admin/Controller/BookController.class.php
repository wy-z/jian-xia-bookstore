<?php
namespace Admin\Controller;
use Base\BaseController;
use Admin\Model\BookModel;

final class Book extends BaseController{

    public function index(){

        //获取搜索条件
        //判断是否有keyword变量且keyword不为空
        if(isset($_GET['keyword']) && !empty($_GET['keyword'])){
            $where = "name LIKE '%{$_GET['keyword']}%'";
            $mode  = "keyword";
        }else if(isset($_GET['bookId']) && !empty($_GET['bookId'])){
            $where = "id = '{$_GET['bookId']}'";
            $mode  = "bookId";
        }else{
            $where = "2>1";
            $mode  = "";
        }
        
        $bookModel = new BookModel;

        //获取图书信息
        $books = $bookModel->fetchAllWithJoin($where);

        
        $this->smarty->assign("books",$books);
        $this->smarty->assign("mode",$mode);
        $this->smarty->display("Book/index.html");
    }

    //显示图书详情页面
    public function detail(){
        $this->accessPage();

        $id = $_GET['id'];

        $bookModel = new BookModel;
        $result = $bookModel->fetchOneWithJoin("book_info.id={$id}");
        
        $this->smarty->assign("book",$result);
        $this->smarty->display("Book/detail.html");
    }

    //显示添加图书页面
    public function add(){
        $this->accessPage();

        $this->smarty->display("Book/add.html");
    }

    //Json添加图书接口
    public function insert(){
        $this->accessJson();
        $bookInfo['name']       =    $_POST['name'];
        $bookInfo['author']     =    $_POST['author'];
        $bookInfo['press']      =    $_POST['press'];
        $bookInfo['press_time']  =   $_POST['pressTime'];
        $bookInfo['price']      =    $_POST['price'];
        $bookInfo['ISBN']       =    $_POST['ISBN'];
        $bookInfo['desc']       =    $_POST['desc'];

        //验证信息是否填写完整
        if(in_array("",$bookInfo)){
            $this->sendJsonMessage("请输入完整信息",1);
        }

        $bookModel = new BookModel;
        if($bookModel->insert($bookInfo)){
            $this->sendJsonMessage("添加成功",0);
        }else{
            $this->sendJsonMessage("添加失败",1);
        }
    }

    //Json删除图书接口
    public function delete(){
        $this->accessJson();

        $id = $_POST['id'];

        $bookModel   = new BookModel;
        if($bookModel->delete("id={$id}")){
            $this->sendJsonMessage("删除成功",0);
        }else{
            $this->sendJsonMessage("删除失败",1);
        }
    }
}