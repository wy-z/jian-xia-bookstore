<?php
namespace Admin\Controller;
use Base\BaseController;
use Admin\Model\UserModel;

final class User extends BaseController{

    public function index(){

        //获取搜索条件
        //判断是否有name变量且name不为空
        if(isset($_GET['name']) && !empty($_GET['name'])){
            $where = "name LIKE '%{$_GET['name']}%'";
            $mode  = "name";
        }else if(isset($_GET['userId']) && !empty($_GET['userId'])){
            $where = "id = '{$_GET['userId']}'";
            $mode  = "userId";
        }else{
            $where = "2>1";
            $mode  = "";
        }

        $userModel = new UserModel;


        //获取用户信息
        $users = $userModel->fetchAllUser($where);


        $this->smarty->assign("users",$users);
        $this->smarty->assign("mode",$mode);
        $this->smarty->display("User/index.html");
    }

    //显示添加用户界面
    public function add(){
        $this->accessPage();

        $this->smarty->display("User/add.html");
    }

    //Json添加用户接口
    public function insert(){
        $this->accessJson();

        //接收前端传过来的数据
        $user['id']      =  $_POST['userId'];
        $user['pwd']     =  md5($_POST['password']);
        $user['name']    =  $_POST['name'];
        $user['class']   =  $_POST['class'];
        $user['status']  =  1;

        $usermodel = new UserModel;

        //查看user数组是否有""(空)值
        if(in_array("",$user)){
            $this->sendJsonMessage("请将信息填写完整",1);
        }

        if($usermodel->rowCount("id={$user['id']}")){
            $this->sendJsonMessage("该用户ID已存在",1);
        }

        if($usermodel->insert($user)){
            $this->sendJsonMessage("添加用户成功",0);
        }else{
            $this->sendJsonMessage("添加用户失败",1);
        }
    }

    //Json删除用户接口
    public function delete(){
        $this->accessJson();

        //获取前端传来的用户Id
        $id  =  $_POST['userId'];

        $userModel   = new UserModel;
        if($userModel->delete("id={$id}")){
            $this->sendJsonMessage("删除成功",0);
        }else{
            $this->sendJsonMessage("删除失败",1);
        }
    }
}