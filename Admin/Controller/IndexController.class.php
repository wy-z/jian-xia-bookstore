<?php
namespace Admin\Controller;
use Admin\Model\CartModel;
use Base\BaseController;
use Admin\Model\UserModel;
use Admin\Model\BookModel;

final class Index extends BaseController{

    public function index(){

        $userModel = new UserModel;
        //查询用户人数
        //重写的rowCount()方法
        $userNum   =  $userModel->rowCount("admin != 1");

        $bookModel = new BookModel;
        //查询图书数量
        $bookNum   =  $bookModel->rowCount();

        $where = "2>1";
        //获取图书信息
        $books = $bookModel->fetchAllWithJoin($where);

        //smarty模板引擎渲染
        //将变量userNum的值赋给smarty模板引擎的userNum变量，以便在模板中使用
        $this->smarty->assign("userNum",$userNum);
        $this->smarty->assign("bookNum",$bookNum);
        $this->smarty->assign("books",$books);

        //指示smarty去渲染模板名为"xxx"的模板文件，可以使用上面的userNum,bookNum来渲染最终的网页。
        $this->smarty->display("Index/index.html"); 
    }

    //书店about页面
    public function about(){
        $this->smarty->display("Index/about.html");
    }

    //购物车页面
    public function cart(){
        $cartModel=new CartModel;

        $cartlist=$cartModel->fetchAll();
        $this->smarty->assign("cartlist",$cartlist);
        $this->smarty->display("Index/cart.html");
    }

    //Json加入购物车接口
    public function insert(){
        $this->accessJson();
        $bookId = $_POST['id'];

        $bookModel=new BookModel;
        // 根据书籍ID在书籍信息表中查找信息
        $bookInfo = $bookModel->getBookInfo($bookId);
        // 解析插入需要的信息
        $cartData = array(
            'book_id' => $bookInfo['id'],
            'name' => $bookInfo['name'],
            'price' => $bookInfo['price'],
            'quantity' => 1,
            'total_price'=>$bookInfo['price']
        );

        $cartModel=new CartModel;
        // 插入到购物车表

        if($cartModel->insert($cartData)){
            $this->sendJsonMessage("添加成功",0);
        }else{
            $this->sendJsonMessage("添加失败",1);
        }
    }

}