<?php
namespace Admin\Model;
use Base\BaseModel;

final class BookModel extends BaseModel{

    //要操作的数据表名
    protected $table = "book_info";

    public function fetchAllWithJoin($where = "2>1",$limit = ""){

        $sql  = "SELECT * FROM {$this->table} ";
        $sql .= "WHERE {$where} ";
        $sql .= "{$limit}";
        
        $result = $this->Db->query($sql);

        //条数为1时,把一维数组转换成二维数组，避免某些foreach循环出错
        if(count($result) == count($result,1) && !empty($result)){
            $result = array($result);
        }
        
        return $result;
    }

    public function fetchOneWithJoin($where = "2>1"){

        $sql  = "SELECT {$this->table}.* FROM {$this->table} ";
        $sql .= "WHERE {$where} ";
        $sql .= "LIMIT 1";

        return $this->Db->query($sql);
    }

    public function getBookInfo($bookId){
        $sql="select * from {$this->table} where id=$bookId";
        return $this->Db->query($sql);
    }

}