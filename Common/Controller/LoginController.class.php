<?php
namespace Common\Controller;
use Base\BaseController;
use Common\Model\UserModel;

//final类，不能被继承，有没有子类
//继承自BaseController

final class  Login extends BaseController{
    
    //检测用户是否登陆，有则导向对应的主页
    private function checkLogin(){
        //isset()函数检测_SESSION数组是否有'userId'键
        if(isset($_SESSION['userId'])){
            $p = $_SESSION['admin'] ="Admin";
            //使用header()函数将页面重定向到对应的主页
            header("location:?p={$p}&c=Index&a=index");
            //die()函数终止脚本执行，防止重复登录
            die();
        }
    }

    public function index(){
        //用户登录--->显示对应的页面
        $this->checkLogin();
        $this->smarty->display("login.html");
    }

    //Json登陆接口
    public function login(){
        header("Content-Type:application/json");
        //htmlentities()将html字符转义，防止类似于sql注入的攻击
        $userId     =   htmlentities($_POST['userId']);     //账号
        //md5()用于加密
        $password   =   md5($_POST['password']);            //密码

        //验证账号密码
        $userModel = new UserModel;
        $where = "id='{$userId}' and pwd='{$password}'";
        $result = $userModel->fetchOne($where);
        if(!empty($result) && $result['status'] == 1){
            //将用户信息存入session
            $_SESSION['userId']           =     $userId;
            $_SESSION['admin']            =     $result['admin'];
            //封装响应信息
            $message = array("message"=>"OK","code"=>0,"admin"=>"{$result['admin']}");
            
        }else if(!empty($result) && $result['status'] == 0){
            $message = array("message"=>"该账户已挂失，请联系管理员解决","code"=>1);
        }
        //返回响应信息
        echo json_encode($message,JSON_UNESCAPED_UNICODE);
    }

    //退出登陆
    public function logout(){
        //session数组置空
        $_SESSION = array();
        //销毁session会话
        session_destroy();
        //重定位到登录界面
        header("location:?p=Common&c=Login&a=index");
    }

}