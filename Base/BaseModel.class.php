<?php
namespace Base;
use Tool\Db;

abstract class BaseModel{

    protected $Db;
    protected $table;   //操作的数据表名

    //构造函数
    public function __construct(){
        $this->Db = Db::getInstance();
    }

    //获取一条数据
    public function fetchOne($where){
        $sql = "SELECT * FROM {$this->table} WHERE {$where}";
        return $this->Db->query($sql);
    }

    //获取全部数据
    public function fetchAll($where = "2>1"){
        $sql = "SELECT * FROM {$this->table} WHERE {$where}";
        $result =  $this->Db->query($sql);

        //条数为1时,把一维数组转换成二维数组，避免某些foreach循环出错
        if(count($result) == count($result,1) && !empty($result)){
            $result = array($result);
        }

        return $result;
    }

    //获取数据条数
    public function rowCount($where = "2>1"){
        $sql = "SELECT * FROM {$this->table} WHERE {$where}";
        return $this->Db->rowCount($sql);
    }

    //插入数据
    public function insert($data){
        //键和值
        $keys = "";
        $values  = "";
        //遍历data的每个键值对
        foreach($data as $key=>$value){
            //将键（字段名）拼接起来
            $keys .= "`{$key}`,";
            //判断字段值是否为空，为空则为null,否则就拼接数据
            if($value == ""){
                $values .= "null,";
            }else{
                $values .= "'{$value}',";
            }
        }
        //将变量末尾的逗号去除掉
        $keys = rtrim($keys,",");
        $values  = rtrim($values,",");
        $sql = "INSERT INTO {$this->table}({$keys}) VALUE({$values})";
        //调用exec()方法，执行SQL语句
        return $this->Db->exec($sql);
    }

    //更新数据
    public function update($data,$where){
        $update = "";
        foreach($data as $key => $value){
            $update .= "`{$key}`='{$value}',";
        }
        $update = rtrim($update,",");
        $sql = "UPDATE {$this->table} SET {$update} WHERE {$where}";
        return $this->Db->exec($sql);
    }

    //删除数据
    public function delete($where){
        $sql = "DELETE FROM {$this->table} WHERE {$where}";
        return $this->Db->exec($sql);
    }
}