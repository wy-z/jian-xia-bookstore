<?php
namespace Base;
use Tool\MySmarty;

abstract class BaseController{

    protected $smarty;
    
    public function __construct(){
        $this->smarty = MySmarty::getInstance();
    }

    //验证页面权限
    protected function accessPage(){
        //用session存储用户信息
        //isset()判断是否有userId键
        if(isset($_SESSION['userId'])){
            if($_SESSION['admin'] == 1 && P == "Admin");
            //登录则重定位到网站首页
            else{
                $p = $_SESSION['admin'] ="Admin";
                header("location:?p={$p}&c=Index&a=index");
                die();
            }
        }else{
            //未登录则重定位到登录界面
            header("location:?p=Common&c=Login&a=index");
            die();
        }
    }

    //验证接口权限
    protected function accessJson(){
        //请求头
        header("Content-Type:application/json");
        if(isset($_SESSION['userId'])){
            if($_SESSION['admin'] == 1 && P == "Admin");
            else{
                $this->sendJsonMessage("操作权限不足",1);
            }
        }else{
            $this->sendJsonMessage("未登陆",1);
        }
    }

    //返回Json信息
    protected function sendJsonMessage($message,$code){
        //转为数组
        $message = array("message"=>$message,"code"=>$code);
        //以json格式返回信息
        echo json_encode($message,JSON_UNESCAPED_UNICODE);
        die();
    }

}