-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2023-12-13 13:34:33
-- 服务器版本： 5.7.26
-- PHP 版本： 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `mybook`
--

-- --------------------------------------------------------

--
-- 表的结构 `book_cart`
--

CREATE TABLE `book_cart` (
  `book_id` tinyint(4) NOT NULL,
  `name` varchar(20) NOT NULL,
  `price` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `quantity` tinyint(3) UNSIGNED NOT NULL,
  `total_price` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `book_cart`
--

INSERT INTO `book_cart` (`book_id`, `name`, `price`, `quantity`, `total_price`) VALUES
(1, '追风筝的人', '89.00', 1, '89.00'),
(9, '活着', '52.98', 1, '52.98'),
(2, '解忧杂货店', '49.88', 1, '49.88'),
(1, '追风筝的人', '89.00', 1, '89.00'),
(4, '白夜行', '29.80', 1, '29.80');

-- --------------------------------------------------------

--
-- 表的结构 `book_info`
--

CREATE TABLE `book_info` (
  `id` int(11) NOT NULL,
  `name` char(30) NOT NULL,
  `author` char(30) NOT NULL,
  `press` char(20) NOT NULL,
  `press_time` char(10) NOT NULL,
  `price` char(10) NOT NULL,
  `ISBN` char(13) NOT NULL,
  `desc` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `book_info`
--

INSERT INTO `book_info` (`id`, `name`, `author`, `press`, `press_time`, `price`, `ISBN`, `desc`) VALUES
(1, '追风筝的人', '[美]卡勒德·胡赛尼', '上海人民出版社', '2006-5', '89.00', '9787208061644', '12岁的阿富汗富家少爷阿米尔与仆人哈桑情同手足。然而，在一场风筝比赛后，发生了一件悲惨不堪的事，阿米尔为自己的懦弱感到自责和痛苦，逼走了哈桑，不久，自己也跟随父亲逃往美国。'),
(2, '解忧杂货店', '[日]东野圭吾', '南海出版公司', '2014-5', '49.88', '9787544270878', '现代人内心流失的东西，这家杂货店能帮你找回——'),
(3, '小王子', '[法]圣埃克苏佩里', '人民文学出版社', '2003-8', '99.68', '9787020042494', '小王子是一个超凡脱俗的仙童，他住在一颗只比他大一丁点儿的小行星上。陪伴他的是一朵他非常喜爱的小玫瑰花。但玫瑰花的虚荣心伤害了小王子对她的感情。小王子告别小行星，开始了遨游太空的旅行。他先后访问了六个行星，各种见闻使他陷入忧伤，他感到大人们荒唐可笑、太不正常。只有在其中一个点灯人的星球上，小王子才找到一个可以作为朋友的人。但点灯人的天地又十分狭小，除了点灯人他自己，不能容下第二个人。在地理学家的指点下，孤单的小王子来到人类居住的地球。'),
(4, '白夜行', '[日]东野圭吾', '南海出版公司', '2008-9', '29.80', '9787544242516', '“只希望能手牵手在太阳下散步”，这个象征故事内核的绝望念想，有如一个美丽的幌子，随着无数凌乱、压抑、悲凉的故事片段像纪录片一样一一还原：没有痴痴相思，没有海枯石烂，只剩下一个冰冷绝望的诡计，最后一丝温情也被完全抛弃，万千读者在一曲救赎罪恶的凄苦爱情中悲切动容……'),
(5, '围城', '钱钟书', '人民文学出版社', '1991-2', '69.74', '9787020024759', '《围城》是钱钟书所著的长篇小说。第一版于1947年由上海晨光出版公司出版。1949年之后，由于政治等方面的原因，本书长期无法在中国大陆和台湾重印，仅在香港出现过盗印本。1980年由作者重新修订之后，在中国大陆地区由人民文学出版社刊印。此后作者又曾小幅修改过几次。《围城》 自从出版以来，就受到许多人的推崇。由于1949年后长期无法重印，这本书逐渐淡出人们的视野。1960年代，旅美汉学家夏志清在《中国现代小说史》(A History of Modern Chinese Fiction)中对本书作出很高的评价，这才重新引起人们对它的关注。'),
(6, '三体', '刘慈欣', '重庆出版社', '2008-1', '93.68', '9787536692930', '文化大革命如火如荼进行的同时。军方探寻外星文明的绝秘计划“红岸工程”取得了突破性进展。但在按下发射键的那一刻，历经劫难的叶文洁没有意识到，她彻底改变了人类的命运。地球文明向宇宙发出的第一声啼鸣，以太阳为中心，以光速向宇宙深处飞驰……'),
(7, '挪威的森林', '[日]村上春树', '上海译文出版社', '2001-2', '68.80', '9787532725694', '这是一部动人心弦的、平缓舒雅的、略带感伤的恋爱小说。小说主人公渡边以第一人称展开他同两个女孩间的爱情纠葛。渡边的第一个恋人直子原是他高中要好同学木月的女友，后来木月自杀了。一年后渡边同直子不期而遇并开始交往。此时的直子已变得娴静腼腆，美丽晶莹的眸子里不时掠过一丝难以捕捉的阴翳。两人只是日复一日地在落叶飘零的东京街头漫无目标地或前或后或并肩行走不止。直子20岁生日的晚上两人发生了性关系，不料第二天直子便不知去向...'),
(8, '嫌疑人X的献身', '[日]东野圭吾', '南海出版公司', '2008-9', '39.99', '9787544241694', '百年一遇的数学天才石神，每天唯一的乐趣，便是去固定的便当店买午餐，只为看一眼在便当店做事的邻居靖子。'),
(9, '活着', '余华', '南海出版公司', '1998-5', '52.98', '9787544210966', '地主少爷福贵嗜赌成性，终于赌光了家业一贫如洗，穷困之中的福贵因为母亲生病前去求医，没想到半路上被国民党部队抓了壮丁，后被解放军所俘虏，回到家乡他才知道母亲已经去世，妻子家珍含辛茹苦带大了一双儿女，但女儿不幸变成了聋哑人，儿子机灵活泼……'),
(10, '红楼梦', '曹雪芹', '人民文学出版社', '1996-12', '59.70', '9787020002207', '《红楼梦》是一部百科全书式的长篇小说。以宝黛爱情悲剧为主线，以四大家族的荣辱兴衰为背景，描绘出18世纪中国封建社会的方方面面，以及封建专制下新兴资本主义民主思想的萌动。结构宏大、情节委婉、细节精致，人物形象栩栩如生，声口毕现，堪称中国古代小说中的经 典。');

-- --------------------------------------------------------

--
-- 表的结构 `borrow_list`
--

CREATE TABLE `borrow_list` (
  `book_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `borrow_date` date NOT NULL,
  `back_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `borrow_list`
--

INSERT INTO `borrow_list` (`book_id`, `user_id`, `borrow_date`, `back_date`) VALUES
(1, 10000, '2018-10-18', '2018-12-18'),
(2, 10000, '2018-10-18', '2018-12-18'),
(5, 10010, '2018-10-13', '2019-04-13'),
(6, 10010, '2018-10-16', '2019-03-16'),
(8, 18888, '2018-10-17', '2019-02-17');

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `pwd` char(128) NOT NULL,
  `name` char(15) DEFAULT NULL,
  `class` char(15) DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1' COMMENT '0为挂失,1为正常',
  `admin` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0为普通用户,1为管理员',
  `last_login_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`id`, `pwd`, `name`, `class`, `status`, `admin`, `last_login_time`) VALUES
(2021210001, '21232f297a57a5a743894a0e4a801fc3', NULL, NULL, 1, 1, '2023-12-05 16:27:22'),
(2021212001, 'e10adc3949ba59abbe56e057f20f883e', '朱元璋', '19943219876', 1, 0, NULL),
(2021212002, 'e10adc3949ba59abbe56e057f20f883e', '徐达', '19943211245', 1, 0, NULL),
(2021212003, 'e10adc3949ba59abbe56e057f20f883e', '汤和', '13567542318', 1, 0, NULL),
(2021212004, 'e10adc3949ba59abbe56e057f20f883e', '朱棣', '18888888888', 1, 0, NULL),
(2021212005, '827ccb0eea8a706c4c34a16891f84e7b', '王语嫣', '13566666666', 1, 0, NULL);

--
-- 转储表的索引
--

--
-- 表的索引 `book_info`
--
ALTER TABLE `book_info`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `borrow_list`
--
ALTER TABLE `borrow_list`
  ADD PRIMARY KEY (`book_id`,`user_id`);

--
-- 表的索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `book_info`
--
ALTER TABLE `book_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
