# 见夏书店

#### 介绍
这是一个用php写的书店管理系统，基于mvc三层架构。

#### 软件架构
前后端混合开发
前端使用html，css，js以及bootstrap等框架。
后端使用php开发，基于mvc三层架构。

#### 安装教程

1.  将所有文件导入到phpstudy的www目录下，同名冲突的可以直接合并。
2. 将数据库文件mybook.sql导入到自己的mysql数据库，建议使用phpstudy自带的数据库管理工具---phpmyadmin

#### 使用说明

1.  账号：2021210001
2.  密码：admin

#### 系统部分截图
![输入图片说明](https://foruda.gitee.com/images/1703034523676307912/6fb794e9_13706342.png "2.png")
![输入图片说明](https://foruda.gitee.com/images/1703034682733347385/a36e71e7_13706342.png "4.png")
![输入图片说明](https://foruda.gitee.com/images/1703034665299005314/fa85d8f4_13706342.png "8.png")
![输入图片说明](https://foruda.gitee.com/images/1703034650676054527/a474f2be_13706342.png "7.png")
![输入图片说明](https://foruda.gitee.com/images/1703034634204083798/9a09661a_13706342.png "6.png")
![输入图片说明](https://foruda.gitee.com/images/1703034617686564290/564fbb8f_13706342.png "3.png")

#### 说明
仅供学习交流使用。

#### 参考项目
https://github.com/horvey/Library-Manager.git